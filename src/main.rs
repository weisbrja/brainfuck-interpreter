use std::collections::HashMap;
use std::env;
use std::fs;
use std::io;
use std::io::Read;
use std::io::Write;
use std::process;

fn main() {
    let usage = || {
        eprintln!("Usage: brainfuck program.bf");
        process::exit(1);
    };

    let mut args = env::args();
    args.next();

    // read given file by filename
    let file = fs::read_to_string(args.next().unwrap_or_else(usage)).unwrap();

    // only one argument should be supplied
    if args.next().is_some() {
        usage();
    }

    // filter for valid instructions
    let instrs: Vec<char> = file
        .chars()
        .filter(|c| matches!(c, '>' | '<' | '+' | '-' | '.' | ',' | '[' | ']'))
        .collect();

    // parse parentheses
    let err = || {
        eprintln!("Parentheses don't match up.");
        process::exit(2);
    };

    let mut open_parens = HashMap::new();
    let mut closed_parens = HashMap::new();

    let mut open_stack = Vec::new();
    for i in 0..instrs.len() {
        let instr = instrs[i];
        if instr == '[' {
            open_stack.push(i);
        } else if instr == ']' {
            let open = open_stack.pop().unwrap_or_else(err);
            open_parens.insert(open, i);
            closed_parens.insert(i, open);
        }
    }
    if !open_stack.is_empty() {
        err();
    }

    // array and pointer
    let mut arr = [0u8; 30_000];
    let mut pointer = 0usize;

    // run
    let mut i = 0;
    while i < instrs.len() {
        match instrs[i] {
            '>' => pointer += 1,
            '<' => pointer -= 1,
            '+' => arr[pointer] = arr[pointer].wrapping_add(1),
            '-' => arr[pointer] = arr[pointer].wrapping_sub(1),
            '[' if arr[pointer] == 0 => i = open_parens[&i],
            ']' if arr[pointer] != 0 => i = closed_parens[&i],
            '.' => {
                print!("{}", arr[pointer] as char);
                io::stdout().flush().unwrap();
            }
            // set arr[pointer] to the inputted char
            ',' => {
                let mut buf = [0];
                io::stdin().read_exact(&mut buf).unwrap_or_else(|_| process::exit(0));
                arr[pointer] = buf[0];
            }
            _ => (),
        }

        // next instruction
        i += 1;
    }
}
